﻿namespace Hospital_PruebasS.Hstorial_Medico
{
    partial class frm_buscadorPaciente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TextBox txtBuscar;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_buscadorPaciente));
            this.label2 = new System.Windows.Forms.Label();
            this.pnl_infoPaciente = new System.Windows.Forms.Panel();
            this.txtVigencia = new System.Windows.Forms.TextBox();
            this.txtApmat = new System.Windows.Forms.TextBox();
            this.txtAppat = new System.Windows.Forms.TextBox();
            this.txtSexo = new System.Windows.Forms.TextBox();
            this.txtEdad = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnHistorial = new System.Windows.Forms.Button();
            this.btnConsulta = new System.Windows.Forms.Button();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlAcciones = new System.Windows.Forms.Panel();
            txtBuscar = new System.Windows.Forms.TextBox();
            this.pnl_infoPaciente.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlAcciones.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtBuscar
            // 
            txtBuscar.ForeColor = System.Drawing.SystemColors.WindowFrame;
            txtBuscar.Location = new System.Drawing.Point(94, 93);
            txtBuscar.Name = "txtBuscar";
            txtBuscar.Size = new System.Drawing.Size(217, 20);
            txtBuscar.TabIndex = 5;
            txtBuscar.Tag = "";
            txtBuscar.Text = "\r\nIngresa NSS del paciente";
            txtBuscar.UseWaitCursor = true;
            // 
            // label2
            // 
            this.label2.Image = ((System.Drawing.Image)(resources.GetObject("label2.Image")));
            this.label2.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label2.Location = new System.Drawing.Point(48, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 38);
            this.label2.TabIndex = 1;
            // 
            // pnl_infoPaciente
            // 
            this.pnl_infoPaciente.Controls.Add(this.txtVigencia);
            this.pnl_infoPaciente.Controls.Add(this.txtApmat);
            this.pnl_infoPaciente.Controls.Add(this.txtAppat);
            this.pnl_infoPaciente.Controls.Add(this.txtSexo);
            this.pnl_infoPaciente.Controls.Add(this.txtEdad);
            this.pnl_infoPaciente.Controls.Add(this.txtNombre);
            this.pnl_infoPaciente.Controls.Add(this.panel2);
            this.pnl_infoPaciente.Controls.Add(this.label3);
            this.pnl_infoPaciente.Location = new System.Drawing.Point(12, 156);
            this.pnl_infoPaciente.Name = "pnl_infoPaciente";
            this.pnl_infoPaciente.Size = new System.Drawing.Size(342, 231);
            this.pnl_infoPaciente.TabIndex = 2;
            // 
            // txtVigencia
            // 
            this.txtVigencia.BackColor = System.Drawing.SystemColors.Control;
            this.txtVigencia.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtVigencia.Enabled = false;
            this.txtVigencia.Location = new System.Drawing.Point(119, 136);
            this.txtVigencia.Name = "txtVigencia";
            this.txtVigencia.ReadOnly = true;
            this.txtVigencia.Size = new System.Drawing.Size(220, 13);
            this.txtVigencia.TabIndex = 7;
            // 
            // txtApmat
            // 
            this.txtApmat.BackColor = System.Drawing.SystemColors.Control;
            this.txtApmat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtApmat.Enabled = false;
            this.txtApmat.Location = new System.Drawing.Point(119, 105);
            this.txtApmat.Name = "txtApmat";
            this.txtApmat.ReadOnly = true;
            this.txtApmat.Size = new System.Drawing.Size(220, 13);
            this.txtApmat.TabIndex = 6;
            // 
            // txtAppat
            // 
            this.txtAppat.BackColor = System.Drawing.SystemColors.Control;
            this.txtAppat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAppat.Enabled = false;
            this.txtAppat.Location = new System.Drawing.Point(119, 74);
            this.txtAppat.Name = "txtAppat";
            this.txtAppat.ReadOnly = true;
            this.txtAppat.Size = new System.Drawing.Size(220, 13);
            this.txtAppat.TabIndex = 5;
            // 
            // txtSexo
            // 
            this.txtSexo.BackColor = System.Drawing.SystemColors.Control;
            this.txtSexo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSexo.Enabled = false;
            this.txtSexo.Location = new System.Drawing.Point(119, 198);
            this.txtSexo.Name = "txtSexo";
            this.txtSexo.ReadOnly = true;
            this.txtSexo.Size = new System.Drawing.Size(220, 13);
            this.txtSexo.TabIndex = 4;
            // 
            // txtEdad
            // 
            this.txtEdad.BackColor = System.Drawing.SystemColors.Control;
            this.txtEdad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEdad.Enabled = false;
            this.txtEdad.Location = new System.Drawing.Point(119, 167);
            this.txtEdad.Name = "txtEdad";
            this.txtEdad.ReadOnly = true;
            this.txtEdad.Size = new System.Drawing.Size(220, 13);
            this.txtEdad.TabIndex = 3;
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.SystemColors.Control;
            this.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombre.Enabled = false;
            this.txtNombre.Location = new System.Drawing.Point(119, 43);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(220, 13);
            this.txtNombre.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(0, 37);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(113, 191);
            this.panel2.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(0, 155);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 31);
            this.label9.TabIndex = 5;
            this.label9.Text = "Sexo\r\n";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(0, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(110, 31);
            this.label8.TabIndex = 4;
            this.label8.Text = "Edad";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(0, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 31);
            this.label7.TabIndex = 3;
            this.label7.Text = "Vigente";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 62);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 31);
            this.label6.TabIndex = 2;
            this.label6.Text = "Apellido Materno";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 31);
            this.label5.TabIndex = 1;
            this.label5.Text = "Apellido Paterno";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 31);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nombre";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(338, 34);
            this.label3.TabIndex = 0;
            this.label3.Text = "Paciente";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnHistorial
            // 
            this.btnHistorial.Location = new System.Drawing.Point(6, 7);
            this.btnHistorial.Name = "btnHistorial";
            this.btnHistorial.Size = new System.Drawing.Size(104, 23);
            this.btnHistorial.TabIndex = 3;
            this.btnHistorial.Text = "Historial Médico";
            this.btnHistorial.UseVisualStyleBackColor = true;
            // 
            // btnConsulta
            // 
            this.btnConsulta.Location = new System.Drawing.Point(229, 7);
            this.btnConsulta.Name = "btnConsulta";
            this.btnConsulta.Size = new System.Drawing.Size(104, 23);
            this.btnConsulta.TabIndex = 4;
            this.btnConsulta.Text = "Generar Consulta";
            this.btnConsulta.UseVisualStyleBackColor = true;
            this.btnConsulta.Click += new System.EventHandler(this.btnConsulta_Click);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(341, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Buscar paciente";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlAcciones
            // 
            this.pnlAcciones.Controls.Add(this.btnHistorial);
            this.pnlAcciones.Controls.Add(this.btnConsulta);
            this.pnlAcciones.Location = new System.Drawing.Point(15, 405);
            this.pnlAcciones.Name = "pnlAcciones";
            this.pnlAcciones.Size = new System.Drawing.Size(336, 33);
            this.pnlAcciones.TabIndex = 6;
            // 
            // frm_buscadorPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.ClientSize = new System.Drawing.Size(366, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(txtBuscar);
            this.Controls.Add(this.pnl_infoPaciente);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pnlAcciones);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_buscadorPaciente";
            this.Text = "Buscador Paciente";
            this.pnl_infoPaciente.ResumeLayout(false);
            this.pnl_infoPaciente.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.pnlAcciones.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnl_infoPaciente;
        private System.Windows.Forms.TextBox txtVigencia;
        private System.Windows.Forms.TextBox txtApmat;
        private System.Windows.Forms.TextBox txtAppat;
        private System.Windows.Forms.TextBox txtSexo;
        private System.Windows.Forms.TextBox txtEdad;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnHistorial;
        private System.Windows.Forms.Button btnConsulta;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlAcciones;
    }
}